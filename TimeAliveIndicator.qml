import QtQuick 2.4

TimeAliveIndicatorForm {


    property int minutesAlive:0
    property int secondsAlive:0
    property var reset:resetTimer
    property alias running:timer.running

    Timer{
        id:timer
        interval: 1000
        repeat:true
        onTriggered: {
            secondsAlive++
        }
    }

    timeContainer.text:{
        var rv = ""

        var minutes = Math.floor(secondsAlive/60);
        minutesAlive+=minutes;
        if(minutesAlive<10){
            rv +="0"+minutesAlive
        }else{
            rv+=minutesAlive
        }

        rv+=":"
        secondsAlive = secondsAlive%60;
        if(secondsAlive< 10){
            rv+="0"+secondsAlive;
        }else{
            rv+=secondsAlive;
        }

        return rv;
    }

    function resetTimer(){
        secondsAlive = 0;
        minutesAlive = 0;
    }
}
