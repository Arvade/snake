import QtQuick 2.4

ScoreIndicatorForm {

    property int score

    scoreContainer.text:formatScore(score)


    function formatScore(score){
        if(score < 10){
            return "000" + score
        }else if(score < 100){
            return "00" + score
        }else if(score < 1000){
            return "0" + score
        }

        return score;
    }
}
