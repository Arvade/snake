import QtQuick 2.4

SnakeForm {
    property int ix:0;
    property int iy:0;
    property bool isFirst:false
    color:isFirst ? "#79CEDC" : "#D5A458"
    x:ix*10
    y:iy*10
}
