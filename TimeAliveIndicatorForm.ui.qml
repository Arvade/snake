import QtQuick 2.4

Item {
    width: 200
    height: 100

    property alias timeContainer:timeContainer

    Text{
        id:timeContainer
        text: "00:00"
        font.bold: true
        fontSizeMode: Text.Fit
        font.pixelSize: 40
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        anchors.fill: parent
        color:"#D5A458"

    }
}
