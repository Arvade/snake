import QtQuick 2.4

Item {
    width: 200
    height: 100

    property alias scoreContainer:scoreContainer

    Text {
        id: scoreContainer
        text: qsTr("0000")
        anchors.rightMargin: 0
        anchors.bottomMargin: 0
        anchors.leftMargin: 0
        anchors.topMargin: 0
        color:"#D5A458"
        font.letterSpacing: 8
        font.bold: true
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        anchors.fill: parent
        font.pixelSize: 40
    }
}
