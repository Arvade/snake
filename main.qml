import QtQuick 2.6
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import QtQml 2.2

Window {
    id: root

    width:500
    height:600
    visible:true
    minimumHeight: 600
    maximumHeight: 600
    minimumWidth: 500
    maximumWidth: 500

    property var component:Qt.createComponent("Snake.qml")
    property string currentDestination
    property int gridSize:50

    property int stepX:0
    property int stepY:0

    property var trail:[]
    property int lenghtOfSnake:5
    property int posX:40
    property int posY:40
    property int points:0

    Column{
        anchors.fill:parent
        Rectangle{
            id:header
            width:parent.width
            height:100
            color:"#0F2043"

            ScoreIndicator{
                anchors.left:parent.left
                score:points
            }

            TimeAliveIndicator{
                id:timeAliveIndicator
                anchors.right: parent.right
            }
        }

        Rectangle{
            id:gameField
            width:root.width
            height: (root.height-header.height)
            visible: true
            border.width:1
            border.color:"#D5A458"
            color:"#0F2043"


            Apple{
                id:apple

                x:ix*10
                y:iy*10
                iy:30
                ix:30
            }

            Timer{
                running:true
                repeat:true
                interval:100
                onTriggered: {
                    game();
                }
            }
            Item{
                focus:true

                Keys.onPressed: {
                    if(event.key == Qt.Key_Up && currentDestination != Qt.Key_Down){
                        currentDestination = event.key
                        stepX = 0
                        stepY = -1
                    }
                    if(event.key == Qt.Key_Down && currentDestination != Qt.Key_Up){
                       currentDestination = event.key
                        stepX = 0
                        stepY = 1
                    }

                    if(event.key == Qt.Key_Left && currentDestination != Qt.Key_Right){
                       currentDestination = event.key
                        stepX = -1
                        stepY = 0
                    }
                    if(event.key == Qt.Key_Right && currentDestination != Qt.Key_Left){
                        currentDestination = event.key
                        stepX = 1
                        stepY = 0
                    }
                    timeAliveIndicator.running = true;
                }
            }
        }
    }


    function game(){
        posX+=stepX
        posY+=stepY

        if(posX > gridSize){
            posX = 0
        }

        if(posY > gridSize){
            posY = 0
        }

        if(posX < 0){
            posX = gridSize
        }

        if(posY < 0){
            posY = gridSize-1;
        }

        for(var i =0 ;i<trail.length;i++){
            var segment = trail[i];
            segment.isFirst = (i == 0) ? true : false


            if(segment.ix === posX && segment.iy === posY){
                reset()
            }

        }

        var segment = component.createObject(gameField, {
                                              ix:posX,
                                              iy:posY
                                          });

        trail.push(segment);
        segment.isFirst = true
        while(trail.length > lenghtOfSnake){
            trail[0].destroy()
            trail.splice(0, 1)
        }


        if(posX == apple.ix && posY == apple.iy){
            lenghtOfSnake++
            points++
            apple.ix = Math.floor(Math.random()*gridSize-1)
            apple.iy = Math.floor(Math.random()*gridSize-1)
        }
    }

    function reset(){
        lenghtOfSnake = 5
        points = 0
        timeAliveIndicator.reset()
    }
}
